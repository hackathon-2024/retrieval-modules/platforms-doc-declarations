# TRA³ TRAnsparency TRAcing TRAck : Collection modules

## Objective

The objective is to develop **collection modules** that can extract the content of specific web pages (API documentation and more generally information from the web pages of platforms dedicated to their transparency documentation) and store the raw HTML content as well as a simplified version in markdown format, so it may be versioned.

<img src="img/hackathon2024-livrable1-EN.png" alt="drawing" width="600"/>

The list of the services and platforms to process is located in the [`platforms.md`](./platforms.md) file.

## Context

The Digital Services Act (DSA), whose provisions are due to come into full effect on 02/17/2024, will make data from online platforms more accessible. This will enable researchers to carry out more in-depth studies than before on the possible systemic risks posed by digital platforms.

This access to data can take a number of forms: either through direct provision by the platforms within their web pages dedicated to their online transparency documentation (transparency report in particular), or through access to programming interfaces (APIs), or through provision of certain data by the European Commission (transparency database).

Rapid access to clear, structured documentation is essential if the research community is to make effective use of these resources. At present, however, such documentation is often sparse and non-standardized.

What's more, by its very nature, this documentation, like the services it describes, is liable to change. This means that we need to develop a tool for periodically consulting, extracting and formatting it and, if necessary, updating it in relation to a previous iteration, while retaining a history of its evolution.

The aim of this challenge is to co-develop an autonomous tool which will be based on modules enabling the collection of part of the documentation from an API or information from a platform's transparency center, and then return the collated results.

## TL;DR

This repository is based on the work of [OpenTermsArchive](https://opentermsarchive.org), all their documentation for [développing a module](https://docs.opentermsarchive.org/contributing-terms/) (best practices, ways of testing, etc...) is relevant and exhaustive in order to address this part of the challenge. You are advised to read the "Contributing Terms" page in its entirety. This document takes up the OTA terminology and highlights from the last page.

This document deals only with the technical aspects of module development. See the [rules](https://hackathon.peren.fr/en/challenge-1/reglement) and the [hackathon website](https://www.hackathon.peren.fr/en/) for the challenge guidelines (platforms studied, etc).

## In practice

### Setup

**Node.js is required.**

Clone the repository and install the dependencies: `npm install`

### Write a module

An example is available : [`declarations/TikTok_Example.json`](./declarations/TikTok_Example.json)

#### [Add a new "service"](https://docs.opentermsarchive.org/contributing-terms/#declaring-a-new-service)

You have to choose:

- [A service name](https://docs.opentermsarchive.org/contributing-terms/#service-name) exposed to end users (field `name`) ;
- [A service ID](https://docs.opentermsarchive.org/contributing-terms/#service-id) exposed to developers (name of `.json`) which follows the pattern `<SERVICE-NAME>_<RESOURCE-NAME>` (e.g. `TikTok_Example`).

#### [Add one or more documents to this service](https://docs.opentermsarchive.org/contributing-terms/#declaring-terms)

1. Add resource name;
2. Resource link;
3. scraping rules.

#### Write scraping selectors

See OTA documentation on how the parsing engine's [various selectors fields work](https://docs.opentermsarchive.org/contributing-terms/#source-document).

The quality of scraping selectors is a factor in the evaluation of modules in this challenge: [guidelines are given here](https://docs.opentermsarchive.org/guidelines/choosing-selectors/).

### Test your module and lint it

To ensure the quality of the module:

1. test it by scraping `npx ota track -s <service_id>` ;
2. check the results available in `./data/versions/` ;
3. lint it `npx ota lint --fix -s <service_id>`.

If necessary, see [the CLI documentation](https://docs.opentermsarchive.org/#cli).

### Contribute

You can contribute by developing collection modules for platforms in the [`platforms.md`](./platforms.md) file for which modules have not been yet implemented.

To contribute, you need to fork the code repository and then create a merge request (MR) from your personal repository to this repository, following the default template. The title of your merge request should be in the form `[S] <PLATFORM-NAME>_<RESOURCE-NAME>` when submitting a new module and `[U] <PLATFORM-NAME>_<RESOURCE-NAME>` or a significant update to an existing module.

A MR must contain **only one module** to be valid.

The MR will then be examined by the organizing team, who will decide on its validity according to the criteria defined in the rules, and will give you feedback if necessary.

You can also propose new platforms that are not on the list via a merge request `[P] <NEW_PLATFORM_1> [<NEW_PLATFORM_2>] [...]` only modifying the [`platforms.md`](./platforms.md) file.

## Evaluation

Each module submitted will be reviewed by our team and  awarded a number of points as defined in Appendix 1 of the [Challenge rules](https://hackathon.peren.fr/en/challenge-1/reglement).

### Module validation

For a module to be valid it must:
- correspond to a URL that is in the [`platforms.md`](./platforms.md) file (or a new URL that we've approved)
- pass the CI
- effectively retrieve the identified resource and only the resource (not headers or footers or anything unrelated).

Once validated by our team, a label will be added with the expected number of points that you'll be awarded once the module is merged (see next section).

### Points
-  Module enabling the retrieval (HTML and markdown format) of documentation relating to a new API endpoint of a new platform (or a new service): **200** points
-  Each additional page added to the module (or update of an existing module): **20** points
-  Module enabling the retrieval (HTML and markdown format) of a transparency report or the content of a transparency documentation web page (retrieval of CSV, PDF or the raw content of the corresponding page): **200** points

You can have a look at the merged MR to get a better understanding of the scoring system.

A leaderboard, updated daily, will be available at: https://hackathon.peren.fr/en/leaderboard.

# Questions? Issues? 

Do not hesitate to open an _issue_ in this repo!

In particular, if you come across a URL that has not yet been documented in [`platforms.md`](./platforms.md), take a moment to submit it through an issue mentionning the platform name, the URL and a short description, if needed.
