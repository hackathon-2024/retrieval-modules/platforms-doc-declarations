# [PLATFORM-NAME]_[RESOURCE-NAME]
%{co_authored_by} 

## Summary of the contribution

Platform name: [PLATFORM-NAME]

Resource type: [API/transparency center/other]

Resource name: [RESOURCE-NAME]

Resource URL: [URL]

Collected content: [A few lines to describe what's being collected]

