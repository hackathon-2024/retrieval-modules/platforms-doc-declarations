# List of platforms and documentation

## Subject to complements

### Amazon Store
- ~~https://assets.aboutamazon.com/cd/28/4d02dd2e41ec8c6d1bc341e9d919/amazon-eu-store-transparency-report-jan-june-2023.pdf~~

### Apple App Store
- https://adrepository.apple.com/fr 
- ~~https://developer.apple.com/support/downloads/Ad-Repository.pdf~~
- https://contentreports.apple.com/
- ~~https://www.apple.com/legal/dsa/transparency/eu/app-store/2310/~~

### Bing 
- https://adlibrary.ads.microsoft.com/
- ~~https://support.microsoft.com/en-gb/topic/eu-digital-services-act-information-6b16b41f-2fa5-4e64-a8d3-033958812642~~
- ~~https://learn.microsoft.com/en-us/advertising/guides/ad-library-api?view=bingads-13~~ 

### Booking
- https://www.booking.com/ad-repository.fr.html
- ~~https://www.booking.com/ad-repository/apidoc.fr.html~~
- ~~https://www.booking.com/content/dsa.fr.html~~
- ~~https://r-xx.bstatic.com/data/mobile/dsa_transparency_report_bf3fdc24.pdf~~

### Facebook / Instagram
- https://developers.facebook.com/docs/
- ~~https://developers.facebook.com/docs/threat-exchange~~
- ~~https://developers.facebook.com/docs/ad-targeting-dataset/~~
- https://developers.facebook.com/docs/content-library-api/guides
- ~~https://developers.facebook.com/docs/fort-pages-api/reference~~ 
- ~~https://developers.facebook.com/docs/graph-api/~~
- ~~https://developers.facebook.com/docs/features-reference~~ 
- ~~https://developers.facebook.com/docs/instagram-api~~/
- ~~https://transparency.fb.com/reports/community-standards-enforcement/~~
- https://transparency.fb.com/reports/regulatory-transparency-reports/
- ~~https://transparency.fb.com/reports/government-data-requests/~~
- ~~https://transparency.fb.com/features/explaining-ranking~~
- ~~https://transparency.fb.com/fr-fr/researchtools/ad-library-tools~~
- https://www.facebook.com/ads/library/report
- ~~https://www.facebook.com/ads/library/api/?source=onboarding~~
- ~~https://help.crowdtangle.com/en/articles/3443476-api-cheat-sheet~~ 
- ~~https://github.com/CrowdTangle/API/wiki~~
- ~~https://fort.fb.com/researcher-apis~~

### Google Maps / Google Play / Google Search / Google Shopping
- ~~https://storage.googleapis.com/transparencyreport/report-downloads/pdf-report-27_2023-8-28_2023-9-10_en_v1.pdf~~
- ~~https://developers.google.com/custom-search/v1/overview~~ 
- https://console.cloud.google.com/marketplace/details/bigquery-public-data/google-ads-transparency-center

### LinkedIn 
- ~~https://about.linkedin.com/transparency/community-report~~
- ~~https://learn.microsoft.com/en-us/linkedin/~~
- ~~https://www.linkedin.com/ad-library/home~~
- ~~https://about.linkedin.com/transparency/government-requests-report~~

### Pinterest
- ~~https://policy.pinterest.com/en/digital-services-act-transparency-report~~
- ~~https://help.pinterest.com/en/article/digital-services-act~~
- ~~https://developers.pinterest.com/~~
- https://developers.pinterest.com/usecase/analyze/
- https://ads.pinterest.com/ads-repository/

### Snapchat
- ~~https://values.snap.com/fr-FR/privacy/transparency~~
- ~~https://values.snap.com/fr-FR/privacy/transparency/european-union~~
- https://adsgallery.snap.com/
- https://www.snap.com/en-US/political-ads 

### Tiktok 
- ~~https://developers.tiktok.com/doc/research-api-codebook/~~
- ~~https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-3/~~
- https://library.tiktok.com/ads
- ~~https://developers.tiktok.com/doc/commercial-content-api-query-ads/~~ 

### X
- ~~https://developer.twitter.com/en/docs/twitter-api~~
- ~~https://transparency.twitter.com/dsa-transparency-report.html~~
- https://business.twitter.com/en/help/ads-policies/product-policies/ads-transparency.html
- https://ads.twitter.com/ads-repository?ref=BTC

### Wikipedia
- ~~https://foundation.wikimedia.org/wiki/Legal:EU_DSA_Userbase_Statistics~~
- ~~https://www.mediawiki.org/wiki/API:Main_page~~

### Youtube
- https://storage.googleapis.com/transparencyreport/report-downloads/pdf-report-27_2023-8-28_2023-9-10_en_v1.pdf ;
- ~~https://developers.google.com/youtube/v3/docs~~
    
