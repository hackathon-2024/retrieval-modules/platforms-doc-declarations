# TRA³ TRAnsparency TRAcing TRAck : Modules de collecte

## Objectif

L'objectif est de développer des **modules de collecte** qui permettent d’extraire le contenu de pages web ciblées (documentation d’API et plus généralement informations des plateformes dédiées à leur documentation de transparence) et d’en stocker le contenu HTML brut ainsi qu’une version simplifiée au format markdown, pouvant être versionnée.

<img src="img/hackathon2024-livrable1.png" alt="drawing" width="600"/>

La liste des plateformes et services à explorer est située dans le fichier [`platforms.md`](./platforms.md).

## Contexte

Le règlement sur les services numériques (RSN), dont l'entrée en application complète des dispositions
est prévue le 17/02/2024, va notamment rendre plus accessibles les données issues des plateformes en
ligne. Ceci permettra aux chercheurs de mener des études plus approfondies qu'auparavant sur les
éventuels risques systémiques que pourraient causer les plateformes numériques.

Cet accès aux données peut se matérialiser sous différentes formes : soit par une mise à disposition
directe par les plateformes au sein de leurs pages web dédiées à leur documentation de transparence
en ligne (rapport de transparence notamment), soit par un accès à des interfaces de programmation
(API), soit par une mise à disposition de certaines données par la Commission Européenne
(transparency database).

Un accès rapide à une documentation claire et structurée est primordial pour une utilisation effective
de ces ressources par la communauté de chercheurs. Or actuellement cette documentation est
souvent éparse et non standardisée.

De plus, par nature, cette documentation, de même que les services qu’elle décrit, est susceptible
d'évoluer. De fait, cela rend nécessaire le développement d'un outil permettant sa consultation
périodique, son extraction, son formatage structuré, et, le cas échéant, sa mise à jour par rapport à une
précédente itération tout en conservant un historique de ses évolutions.

L'objectif de ce challenge est de co-développer un outil
autonome qui s'appuiera sur des modules permettant de collecter une partie de la documentation
d'une API ou d’informations depuis le centre de transparence d'une plateforme, puis de renvoyer les
résultats collectés.

## TL;DR

Ce dépôt s'appuie sur le travail d'[OpenTermsArchive](https://opentermsarchive.org/fr), toute leur documentation pour [développer un module](https://docs.opentermsarchive.org/contributing-terms/) (bonnes pratiques, manières de tester, etc...) est pertinente et exhaustive afin de traiter cette partie du challenge. Il est conseillé de lire la page _[Contributing Terms](https://docs.opentermsarchive.org/contributing-terms/)_ en entier. Ce document reprend la terminologie d'OTA et les points saillants de cette dernière page.

Ce document traite uniquement de l'aspect technique de développement des modules. Consulter le [règlement](https://hackathon.peren.fr/fr/challenge-1/reglement) et le [site du hackathon](https://hackathon.peren.fr/fr/) pour les consignes du challenge (plateformes étudiées, etc).

## En pratique

### Configuration

**Node.js est requis.**

Cloner le dépôt puis installer les dépendances : `npm install`.

### Écrire un module

Un exemple est disponible : [`declarations/TikTok_Example.json`](./declarations/TikTok_Example.json)

#### [Ajouter un nouveau "service"](https://docs.opentermsarchive.org/contributing-terms/#declaring-a-new-service)

Il faut choisir :

- [Un nom de service](https://docs.opentermsarchive.org/contributing-terms/#service-name) exposé aux utilisateurs finaux (champ `name`) ;
- [Un identifiant de service](https://docs.opentermsarchive.org/contributing-terms/#service-id) exposé aux développeurs (nom du `.json`). Il doit de la forme `<NOM DU SERVICE>_<NOM DE LA RESSOURCE>` (par exemple `TikTok_Example`).

#### [Ajouter un ou plusieurs documents pour ce service](https://docs.opentermsarchive.org/contributing-terms/#declaring-terms)

1. Ajouter le nom de la ressource ;
2. Le lien de la ressource ;
3. Les différentes règles pour le scraping.

#### Écrire les sélecteurs de scraping

Voir la documentation d'OTA sur le fonctionnement des [différents champs de sélecteurs](https://docs.opentermsarchive.org/contributing-terms/#source-document) du _parsing engine_.

La qualité des sélecteurs de scraping est un facteur d'évaluation des modules dans ce challenge : [des lignes directrices sont données ici](https://docs.opentermsarchive.org/guidelines/choosing-selectors/).

### Tester son module et le _linter_

Afin de s'assurer de la qualité du module :

1. le tester en lançant le scraping `npx ota track -s <service_id>` ;
2. contrôler les résultats disponibles dans `./data/versions/` ;
3. le _linter_ `npx ota lint --fix -s <service_id>`.

Si besoin, voir [la documentation de la CLI](https://docs.opentermsarchive.org/#cli).

### Contribuer

Vous pouvez contribuer en développant des modules de collecte pour des plateformes présentes dans le fichier [`platforms.md`](./platforms.md) pour lesquelles des modules n'ont pas encore été implémentés.

Pour contribuer, il est nécessaire de forker le dépôt de code puis de créer une _merge request_ depuis votre dépôt personnel vers ce dépôt en suivant le template proposé par défaut. Le titre de votre _merge request_ devra être sous la forme `[S] <PLATFORM-NAME>_<RESOURCE-NAME>` lorsque vous soumettez un nouveau module et `[U] <PLATFORM-NAME>_<RESOURCE-NAME>` pour une mise à jour significative d'un module existant.

Une MR ne doit contenir **qu'un seul module** pour être valide.

La MR sera ensuite examinée par l'équipe organisatrice qui décidera de sa validité selon les critères définis dans le règlement et vous fera des retours, le cas échéant.

Vous pouvez également proposer de nouvelles plateformes qui ne sont pas dans la liste via une _merge request_ `[P] <NEW_PLATFORM_1> [<NEW_PLATFORM_2>] [...]`, modifiant uniquement le fichier [`platforms.md`](./platforms.md).

## Evaluation

Chaque module soumis sera examiné par notre équipe qui en déterminera la validité et, lorsqu'il est valide, donnera lieu à un nombre de points tel que défini dans l'Annexe 1 du [Règlement du Challenge](https://hackathon.peren.fr/fr/challenge-1/reglement)

### Validation des modules

Un module est déclaré valide s'il remplit les conditions cumulatives suivantes :
- il correspond à une URL présente dans la liste issue du fichier [`platforms.md`](./platforms.md) (ou il s'agit d'une nouvelle URL que nous avons approuvée) ;
- le module passe la CI ;
- il permet de récupérer la ressource cible et uniquement la ressource concernée (et non le reste de la page web sans rapport).

Une fois validé par notre équipe, un _label_ sera ajouté à la _merge request_ qui indiquera le nombre de points (voir section suivante) correspondant au module développé, attribués lorsqu'il sera fusionné en production.

### Attribution des points

- Module permettant la récupération (format HTML et markdown) de la documentation relative à un nouvel endpoint d’une API d’une nouvelle plateforme (ou d'un nouveau service): **200** points
-  Chaque page suivante ajoutée au module (ou ajout à un module existant) : **20** points
- Module permettant la récupération (dans un format HTML et markdown) d'un rapport de transparence ou du contenu d’une page web de documentation de transparence (récupération du CSV, PDF ou du contenu brut de la page correspondante) : **200** points

Vous pouvez consulter les _merge requests_ fusionnées afin d'avoir un apercu concret de l'utilisation de ce système de points.

Un leaderboard, actualisé tous les jours, sera disponible à l'adresse suivante : https://hackathon.peren.fr/fr/leaderboard.

# Un problème ? Une question ?

N'hésitez pas à ouvrir une issue sur ce dépôt !

En particulier, si vous trouvez une URL qui n'est pas documentée dans [`platforms.md`](./platforms.md), veuillez créer une _issue_ avec le nom de la plateforme concernée, l'URL associée et une courte description.
